Como rodar:
===========

-Clona

-Instala o tensorflow 2, pillow, wandb e as outras coisas que estiverem faltando

-configura wandb caso não tenha configurado antes ou coloca em modo offline (veja na documentação deles)

-roda o train.py

o script vai treinar a rede e salvar na pasta model tanto em formato h5 quanto em SaveModel


Changelog:
=========

aumentei a quantidade de épocas pra 10k

diminuí o tamanho de input pra 128x128x1

adicionei uma função de preprocessamento que inverte a imagem

coloquei hook pro Weights and Biases

Adicionei a conversão pra .tflite

Atualizei o código pra usar o tensorflow 2 (Código inicial em https://github.com/xiaochus/MobileNetV2)

Adicionei save em formato SaveModel, que é o novo padrão de salvar modelo pro 

tensorflow 2

Aumentei a paciência do EarlyStop

Setei o número de classes para 2
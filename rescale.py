from PIL import Image
import os
import pathlib

source_folder = 'source'
destination_folder = 'destination'

for folder in os.listdir(source_folder):
    if os.path.isdir(os.path.join(source_folder, folder)):
        # ensures the destination folder exists
        pathlib.Path(os.path.join(destination_folder, folder)).mkdir(parents=True, exist_ok=True)

        for filename in os.listdir(os.path.join(source_folder, folder)):
            file_path = os.path.join(source_folder, folder, filename)
            if os.path.isfile(file_path):
                img = Image.open(file_path).convert('L')  # luma only (grayscale)
                # img.show()
                resized_image = img.resize((128, 128))
                # resized_image.show()
                filename, _ = os.path.splitext(filename)
                filename = '_' + filename + '.jpg'
                resized_image.save(os.path.join(destination_folder, folder, filename))

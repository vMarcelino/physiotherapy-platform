from typing import Callable
import tensorflow as tf
from PIL import Image
import numpy as np
import cv2

cap = cv2.VideoCapture(0)

use_tflite = True
model_predict: Callable
if use_tflite:
    interpreter = tf.lite.Interpreter(model_path="model/model.tflite")
    interpreter.allocate_tensors()

    # Get input and output tensors.
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    def _model_predict(input_data):
        interpreter.set_tensor(input_details[0]['index'], input_data)
        interpreter.invoke()
        output_data = interpreter.get_tensor(output_details[0]['index'])
        return output_data

    model_predict = _model_predict

else:
    model = tf.keras.models.load_model('model/saved')
    model_predict = model.predict

mapping = ['closed', 'open']


def predict(frame):
    # size = frame.shape[0:2]
    img = Image.fromarray(frame).convert('L').resize((128, 128))
    data = np.reshape(img, (1, 128, 128, 1)) / 255
    prediction = model_predict(data.astype('Float32'))
    print(mapping[int(prediction.argmax(axis=-1)[0])], *map(float, prediction[0]), ' ' * 10, end='\r')
    img = np.reshape(img, (128, 128, 1)) / 255
    return img


while True:
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    img = predict(frame)

    # Display the resulting frame
    cv2.imshow('frame', img)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()

import os
import sys
import argparse
import pandas as pd
import numpy as np
import random

import tensorflow as tf
# tf.config.gpu.set_per_process_memory_fraction(0.75)
# tf.config.gpu.set_per_process_memory_growth(True)

from model import MobileNetv2

from tensorflow.keras.optimizers import Adam
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.layers import Conv2D, Reshape, Activation
from tensorflow.keras.models import Model

import wandb
from wandb.keras import WandbCallback
wandb.init(project="physiotherapy-platform", config=dict(MobileNetv2_alpha=0.3))
# wandb.save('*')


def main(argv):
    parser = argparse.ArgumentParser()
    # Required arguments.
    parser.add_argument("--classes", default=2, help="The number of classes of dataset.")
    # Optional arguments.
    parser.add_argument("--size", default=128, help="The image size of train sample.")
    parser.add_argument("--batch", default=32, help="The number of train samples per batch.")
    parser.add_argument("--epochs", default=10000, help="The number of train iterations.")
    parser.add_argument("--weights", default=False, help="Fine tune with other weights.")
    parser.add_argument("--tclasses", default=0, help="The number of classes of pre-trained model.")

    args = parser.parse_args()

    train(int(args.batch), int(args.epochs), int(args.classes), int(args.size), args.weights, int(args.tclasses))


def generate(batch_size, image_size):
    #  Using the data Augmentation in traning data
    train_data_directory = 'destination'

    def image_inverter(img):
        if random.choice((True, False)):
            return 1 - img
        else:
            return img

    train_datagen = ImageDataGenerator(rescale=1. / 255,
                                       shear_range=0.2,
                                       zoom_range=0.2,
                                       rotation_range=180,
                                       width_shift_range=0.2,
                                       height_shift_range=0.2,
                                       horizontal_flip=True,
                                       vertical_flip=True,
                                       validation_split=0.1,
                                       preprocessing_function=image_inverter)

    train_generator = train_datagen.flow_from_directory(train_data_directory,
                                                        target_size=(image_size, image_size),
                                                        batch_size=batch_size,
                                                        color_mode='grayscale',
                                                        class_mode='categorical',
                                                        subset='training')

    validation_generator = train_datagen.flow_from_directory(train_data_directory,
                                                             target_size=(image_size, image_size),
                                                             batch_size=batch_size,
                                                             color_mode='grayscale',
                                                             class_mode='categorical',
                                                             subset='validation')

    return train_generator, validation_generator


def fine_tune(num_classes, weights, model):
    """Re-build model with current num_classes.
    # Arguments
        num_classes, Integer, The number of classes of dataset.
        tune, String, The pre_trained model weights.
        model, Model, The model structure.
    """
    model.load_weights(weights)

    x = model.get_layer('Dropout').output
    x = Conv2D(num_classes, (1, 1), padding='same')(x)
    x = Activation('softmax', name='softmax')(x)
    output = Reshape((num_classes, ))(x)

    model = Model(inputs=model.input, outputs=output)

    return model


def train(batch_size: int, epochs: int, num_classes: int, image_size: int, weights: str, trained_classes: int):
    """Train the model.
    # Arguments
        batch_size: classesnteger, The number of train samples per batch.
        epochs: Integer, The number of train iterations.
        num_classes, Integer, The number of classes of dataset.
        image_size: Integer, image size.
        weights, String, The pre_trained model weights.
        trained_classes, Integer, The number of classes of pre-trained model.
    """

    train_generator, validation_generator = generate(batch_size, image_size)

    if weights:
        model = MobileNetv2((image_size, image_size, 1), trained_classes, wandb.config.MobileNetv2_alpha)
        model = fine_tune(num_classes, weights, model)
    else:
        model = MobileNetv2((image_size, image_size, 1), num_classes, wandb.config.MobileNetv2_alpha)

    optimizer = Adam()
    earlystop = EarlyStopping(monitor='loss', patience=300, verbose=2, mode='min')
    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])

    history = model.fit(train_generator,
                     validation_data=validation_generator,
                     steps_per_epoch=None,
                     validation_steps=None,
                     epochs=epochs,
                     callbacks=[earlystop, WandbCallback()])

    if not os.path.exists('model'):
        os.makedirs('model')

    df = pd.DataFrame.from_dict(history.history)
    df.to_csv('model/history.csv', encoding='utf-8', index=False)
    model.save('model/model.h5')
    model.save('model/saved')
    converter = tf.lite.TFLiteConverter.from_keras_model(model)
    tflite_model = converter.convert()

    # Save the TF Lite model.
    with tf.io.gfile.GFile('model/model.tflite', 'wb') as f:
        f.write(tflite_model)

    with tf.io.gfile.GFile(os.path.join(wandb.run.dir, 'model.tflite'), 'wb') as f:
        f.write(tflite_model)

    model.save(os.path.join(wandb.run.dir, "model.h5"))
    model.save(os.path.join(wandb.run.dir, "SavedModel"))


if __name__ == '__main__':
    main(sys.argv)